import { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import NavBar from './components/NavBar';
import Home from './pages/Home';
import AdminView from "./pages/AdminView";
import Products from './pages/Products';
import Register from './pages/Register';
import SpecificProduct from './pages/SpecificProduct';
import Cart from './pages/Cart';
import Order from './pages/Order';
import Login from './pages/Login';
import Error from './pages/Error';

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { UserProvider } from './UserContext';


export default function App() {

  // global user state
  const [user, setUser ] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
    setUser({
      id: null,
      isAdmin: null
    })
  }
  
  useEffect(() => {
    fetch("http://localhost:4000/users/:userId/userDetails", {
      headers: { 
        Authorization: `Bearer ${ localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }else{
        setUser({
          id: null,
          isAdmin: null 
        })
      }
    })

  }, [])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <NavBar/>
        <Routes>
          <Route path="/" element={<Home/>} />
          <Route path="/adminView" element={<AdminView />} />
          <Route path="/products" element={<Products/>} />
          <Route path="/products/:productId" element={<SpecificProduct/>} />
          <Route path="/login" element={<Login/>} />
          <Route path="/cart" element={<Cart/>} />
          <Route path="/orders" element={<Order/>} />
          <Route path="/register" element={<Register/>} />
          <Route path="*" element={<Error />} />
        </Routes>
      </Router>
    </UserProvider>
  );
}
