import { useState, useEffect } from 'react';
import { Card, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Product({ productProp }) {
  const { _id, name, description, price } = productProp;

  const [orderCount] = useState(30);
  const [setIsDisabled] = useState(false);

  useEffect(() => {
    if (orderCount === 0) {
      setIsDisabled(true);
    }
  }, [orderCount, setIsDisabled]); // Include setIsDisabled in the dependency array

  return (
    <Container>
      <Card className="text-center">
        <Card.Header className="font bg-pink text-white">{name}</Card.Header>
        <Card.Body className="bg-light">
          <Card.Text>
            {description}
            <br />
            PHP {price}
          </Card.Text>

          <Link className="btn btn-warning" to={`/products/${_id}`}>
            Order Now!
          </Link>
        </Card.Body>
      </Card>
    </Container>
  );
}
