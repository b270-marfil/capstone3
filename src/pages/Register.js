import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Card, Row, Col } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    setIsActive(
      firstName !== '' &&
        lastName !== '' &&
        email !== '' &&
        mobileNumber.length === 11 &&
        password !== ''
    );
  }, [firstName, lastName, email, mobileNumber, password]);

  function registerUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNumber: mobileNumber,
        password: password,
      }),
    })
      .then((res) => {
        if (res.ok) {
          setFirstName('');
          setLastName('');
          setEmail('');
          setMobileNumber('');
          setPassword('');

          Swal.fire({
            title: 'Registration successful',
            icon: 'success',
            text: 'Welcome to Saints Linen!',
          });

          navigate('/login');
        } else {
          throw new Error('Something went wrong.');
        }
      })
      .catch((error) => {
        Swal.fire({
          title: 'Something went wrong.',
          icon: 'error',
          text: 'Please try again.',
        });
      });
  }


  return user.id !== null ? (
    <Navigate to="/product" />
  ) : (
    <Container>
      <Row className="justify-content-center align-items-center form-margin">
        <Col xs={12} sm={8} md={6} lg={4}>
          <Card className="bg-info text-white">
            <Card.Body>
              <Form className="mt-3" onSubmit={(e) => registerUser(e)}>
                <Form.Group>
                  <Form.Label>First Name:</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter First Name"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group>
                  <Form.Label>Last Name:</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter Last Name"
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group>
                  <Form.Label>Mobile Number:</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter Mobile Number"
                    value={mobileNumber}
                    onChange={(e) => setMobileNumber(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group>
                  <Form.Label>Email Address:</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter your email address"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group>
                  <Form.Label>Password:</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Enter Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                  />
                </Form.Group>

                <div className="row justify-content-center align-items-center button-margin">
                  <Button
                    variant="warning"
                    type="submit"
                    id="submitBtn"
                    disabled={!isActive}
                  >
                    Submit
                  </Button>
                </div>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
